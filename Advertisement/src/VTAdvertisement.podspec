#
#  Be sure to run `pod spec lint VTAdvertisement.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  s.name         = "VTAdvertisement"
  s.version      = "0.1"
  s.summary      = "Advertisement banner on app lunch"
  s.homepage     = "https://bitbucket.org/totsky_vv/advertisement"

  s.license      = { :type => "MIT", :file => "LICENSE" }

  s.author             = { "Valentin Totsky" => "vtotskyi@gmail.com" }

  s.platform     = :ios

  s.source       = { :git => "https://bitbucket.org/totsky_vv/advertisement", :tag => "0.1" }

  s.source_files  = "Advertisement/src/*.{h,m}"

  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
  # s.dependency "JSONKit", "~> 1.4"

end
