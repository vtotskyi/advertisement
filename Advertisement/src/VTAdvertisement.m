//
//  VTAdvertisement.m
//  Advertisement
//
//  Created by Валентин Тоцкий on 08.04.14.
//  Copyright (c) 2014 Valentin Totskyi. All rights reserved.
//

#import "VTAdvertisement.h"

#define VT_ADVERTISEMENT_URL @"http://3colorcat.com/advertisement/index.php"

#define VT_PARAMS_BUNDLE_KEY    @"bundleID"
#define VT_PARAMS_DEVICE_KEY    @"device"
#define VT_PARAMS_LANGUAGE_KEY  @"language"

typedef enum{
    VTAdvertisementStatusShow,
    VTAdvertisementStatusStop
} VTAdvertisementStatus;

@interface VTAdvertisement()

@property (nonatomic) VTAdvertisementStatus status;
@property (nonatomic, strong) NSURL* imageRemoteURL;
@property (nonatomic, strong) NSURL* imageLocalURL;
@property (nonatomic, strong) NSURL* url;
@property (nonatomic) int repeat;

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic,strong) UIButton *closeButton;

@end

@implementation VTAdvertisement

+ (void) show{
    static VTAdvertisement* ad = nil;
    
    if(!ad)
    {
        ad = [[VTAdvertisement alloc] init];
    }
        
    double delayInSeconds = 0.001;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [ad _recoverDefaults];
        [ad _showAvailable];
        [ad _updateStatus];
    });

}

- (void) _recoverDefaults{
    
    NSString* statusString = [[NSUserDefaults standardUserDefaults] objectForKey:@"vt_advertisement_status"];
    if( [statusString isEqualToString:@"show"] )
        _status = VTAdvertisementStatusShow;
    else
        _status = VTAdvertisementStatusStop;
    
    NSString* remoteURLString = [[NSUserDefaults standardUserDefaults] objectForKey:@"vt_advertisement_remote_url"];
    _imageRemoteURL = [NSURL URLWithString:remoteURLString];
    
    NSString* localURLString = [[NSUserDefaults standardUserDefaults] objectForKey:@"vt_advertisement_local_url"];
    _imageLocalURL = [NSURL URLWithString:localURLString];
    
    NSString* urlString = [[NSUserDefaults standardUserDefaults] objectForKey:@"vt_advertisement_url"];
    _url = [NSURL URLWithString:urlString];
    
    _repeat = [[NSUserDefaults standardUserDefaults] integerForKey:@"vt_advertisement_repeat"];
    _repeat = MAX(0, _repeat);
    _repeat = MIN(100, _repeat);
    
    NSLog(@"VT AD: recover defaults:\nStatus = %@\nRemote URL = %@,\nLocal URL = %@,\nURL = %@,\nrepeat = %i", statusString, remoteURLString, localURLString, urlString, _repeat);
}

- (void) _saveDefaults{
    NSString* status = @"";
    switch (_status) {
        case VTAdvertisementStatusShow: status = @"show";  break;
        default: status = @"stop"; break;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:status forKey:@"vt_advertisement_status"];
    
    if(_imageRemoteURL.absoluteString){
        [[NSUserDefaults standardUserDefaults] setObject:_imageRemoteURL.absoluteString forKey:@"vt_advertisement_remote_url"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"vt_advertisement_remote_url"];
    }
    
    if(_imageLocalURL.absoluteString){
        [[NSUserDefaults standardUserDefaults] setObject:_imageLocalURL.absoluteString forKey:@"vt_advertisement_local_url"];
    } else {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"vt_advertisement_local_url"];
    }
    
    if (_url.absoluteString) {
        [[NSUserDefaults standardUserDefaults] setObject:_url.absoluteString forKey:@"vt_advertisement_url"];
    }
    
    [[NSUserDefaults standardUserDefaults] setInteger:_repeat forKey:@"vt_advertisement_repeat"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
     NSLog(@"VT AD: saved defaults:\nStatus = %@\nRemote URL = %@,\nLocal URL = %@,\nURL = %@,\nrepeat = %i", status, _imageRemoteURL.absoluteString, _imageLocalURL.absoluteString, _url.absoluteString, _repeat);
}

- (void) _showAvailable{
    if(_repeat>0){
        if(_imageLocalURL.absoluteString){
            UIWindow *window = [[UIApplication sharedApplication] keyWindow];
            
            UIImage* image = [UIImage imageWithContentsOfFile:[_imageLocalURL path]];
            if(!image){
                NSLog(@"VT AD: unable to show ad, no image data");
                return;
            }
            
            _imageView = [self _advertImageViewWithFrame:window.bounds andImage:image];
            _closeButton = [self _closeButton];            
            
            [window addSubview:_imageView];
            [window addSubview:_closeButton];
            
            _imageView.alpha = 0;
            [UIView animateWithDuration:0.25 animations:^{
                _imageView.alpha = 1;
            }];
            _repeat-=1;
            [self _saveDefaults];
        } else {
            NSLog(@"VT AD: unable to show ad, no image cached");
        }
    } else {
        NSLog(@"VT AD: unable to show ad, repeat = 0");
    }
}

- (void) _updateStatus{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    
    [manager GET:VT_ADVERTISEMENT_URL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject){
        NSDictionary* dict = (NSDictionary*) responseObject;
        if( [dict isKindOfClass:[NSDictionary class] ] ){
            
            NSString* status = [dict objectForKey:@"status"];
            if([status isKindOfClass:[NSString class] ]){
                if( [status isEqualToString:@"show"] )
                    _status = VTAdvertisementStatusShow;
                else
                    _status = VTAdvertisementStatusStop;
            }
            
            NSString* remoteURLString = [dict objectForKey:@"image"];
            if(! [remoteURLString isEqualToString:_imageRemoteURL.absoluteString] ){
                _imageRemoteURL = [NSURL URLWithString:remoteURLString];
                _imageLocalURL = nil;
            
            
                NSNumber* repeat = [dict objectForKey:@"repeat"];
                if([repeat isKindOfClass:[NSNumber class] ]){
                    _repeat = [repeat integerValue];
                } else {
                    _repeat = 0;
                }
                
                _repeat = MAX(0, _repeat);
                _repeat = MIN(100, _repeat);

                _url = [NSURL URLWithString:[dict objectForKey:@"url"]];
            }
            
            [self _saveDefaults];
            if(!_imageLocalURL) [self _updateImage];
            
        } else {
            NSLog(@"VT AD: failed update status, not nsdictionary");
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"VT AD: failed update status %@", error.localizedDescription);
    }];
}

- (void) _updateImage{
    if(_imageRemoteURL.absoluteString){
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        
    AFImageResponseSerializer *serializer = [[AFImageResponseSerializer alloc] init];
    serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/pjpeg"];
    serializer.acceptableContentTypes = [serializer.acceptableContentTypes setByAddingObject:@"image/jpg"];
    manager.responseSerializer = serializer;

    NSDictionary *params = [self _paramsForGetRequest];
    [manager GET:_imageRemoteURL.absoluteString parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        UIImage* image = (UIImage*) responseObject;
        if([image isKindOfClass:[UIImage class] ]){
            NSString* path = [NSHomeDirectory() stringByAppendingString:@"/Library/Caches/vt_advertisement_image.png"];
            _imageLocalURL = [NSURL fileURLWithPath:path];
            
            BOOL ok = [[NSFileManager defaultManager] createFileAtPath:path
                                                              contents:nil attributes:nil];
            
            if (!ok) {
                NSLog(@"VT AD: Error creating image file %@", path);
            }
            else {
                NSFileHandle* myFileHandle = [NSFileHandle fileHandleForWritingAtPath:path];
                [myFileHandle writeData:UIImagePNGRepresentation(image)];
                [myFileHandle closeFile];
                [self _saveDefaults];
            }
        } else {
             NSLog(@"VT AD: failed update image, not image");
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"VT AD: failed update image %@", error.localizedDescription);
    }];

    }
}

#pragma mark - 

- (UIImageView*) _advertImageViewWithFrame:(CGRect)frame andImage:(UIImage*)image
{
    if (!image)
    return nil;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:frame];
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    [imageView setImage:image];
    imageView.userInteractionEnabled = YES;
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
    gestureRecognizer.delegate = self;
    [imageView addGestureRecognizer:gestureRecognizer];
    
    return imageView;
}

- (UIButton*) _closeButton
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setFrame:CGRectMake(5, 20, 44, 44)];
    [button setTitle:@"✕" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:0.3]];
    button.titleLabel.font = [UIFont systemFontOfSize:30];
    
    button.layer.cornerRadius = button.frame.size.width/2;
    button.clipsToBounds = YES;
    button.layer.borderWidth = 1;
    button.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [button addTarget:self action:@selector(closeAction:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

#pragma mark - Parameters for GET request -

- (NSDictionary*) _paramsForGetRequest
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setObject:[self _bundle] forKey:VT_PARAMS_BUNDLE_KEY];
    [params setObject:[self _device] forKey:VT_PARAMS_DEVICE_KEY];
    [params setObject:[self _language] forKey:VT_PARAMS_LANGUAGE_KEY];
    
    return [NSDictionary dictionaryWithDictionary:params];
}

- (NSString*) _bundle
{
    return [[NSBundle mainBundle] bundleIdentifier];;
}

- (NSString*) _device
{
    if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone))
    {
        if ([[UIScreen mainScreen] bounds].size.height > 480.0f)
            return @"iPhone5";
        return @"iPhone4";
    }
    
    return @"iPad";
}

- (NSString*) _language
{
    return [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
}

#pragma mark - Action -

- (void) closeAction:(UIButton*)sender
{
    [UIView animateWithDuration:0.5 animations:^{
        _imageView.alpha = 0.0;
        _closeButton.alpha = 0.0;
    } completion:^(BOOL finished) {
        [_imageView removeFromSuperview];
        [_closeButton removeFromSuperview];
    }];
}

- (void) imageTapped:(UIGestureRecognizer*)recognizer
{
    if (_url && [[UIApplication sharedApplication] canOpenURL:_url])
        [[UIApplication sharedApplication] openURL:_url];
}

@end
