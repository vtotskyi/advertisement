//
//  VTAdvertisement.h
//  Advertisement
//
//  Created by Валентин Тоцкий on 08.04.14.
//  Copyright (c) 2014 Valentin Totskyi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

@interface VTAdvertisement : NSObject <UIGestureRecognizerDelegate>

+ (void) show;

@end
