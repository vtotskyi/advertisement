//
//  VTAppDelegate.h
//  Advertisement
//
//  Created by Валентин Тоцкий on 08.04.14.
//  Copyright (c) 2014 Valentin Totskyi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VTAdvertisement.h"

@interface VTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
