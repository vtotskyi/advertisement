Pod::Spec.new do |s|

  s.name         = "Advertisement"
  s.version      = "0.0.2"
  s.summary      = "The mechanism of showing target advertisement."



  s.homepage     = "http://inteza.net"

  s.license      = { :type => "MIT", :file => "LICENSE" }


  s.author             = { "Valentin Totsky" => "vtotskyi@gmail.com" }
  s.platform     = :ios, "7.0"

  s.source       = { :git => "https://bitbucket.org/totsky_vv/advertisement", :tag =>  s.version.to_s }

  s.source_files  = "Advertisement/src/*.{h,m}"
  s.public_header_files = "Advertisement/src/*.h"
  s.requires_arc = true

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }
    s.dependency "AFNetworking"

end
